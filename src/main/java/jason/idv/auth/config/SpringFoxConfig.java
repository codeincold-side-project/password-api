package jason.idv.auth.config;

import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SpringFoxConfig {

    @Value(value = "${swagger.host.url:}")
    private String swaggerHostURL;

    @Bean
    public Docket api() {
        Docket docket = new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(new ApiInfoBuilder().title("API Documentation").build())
                .select()
                .apis(RequestHandlerSelectors.basePackage("jason.idv.auth.controller"))
                .paths(PathSelectors.any())
                .build();

        if (!Strings.isEmpty(swaggerHostURL)) {
            docket.host(swaggerHostURL);
        }

        return docket;
    }
}
