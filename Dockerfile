FROM amazoncorretto:17
VOLUME /tmp
COPY /target/*.jar password-api.jar
ENTRYPOINT ["java","-jar","-Dspring.profiles.active=prod","/password-api.jar"]