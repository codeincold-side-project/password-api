# Password Validation

It's a practice project to use maven designing a simple password strength validation server with comprehensive tests. You can test API on Swagger UI page. After validated you will get a `PasswordResult` class which contains `passed` and `failedReasons` fields. 
Also, You can find out unit test cases and integration test cases in the test package along with testing coverage statistic by Jacoco.

## Demo

- [Swagger UI](https://jasonjiang.info/password-api/swagger-ui/#/)
- [Jacoco Report](https://jasonjiang.info/password-api/jacoco/index.html)
